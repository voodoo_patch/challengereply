﻿using Priority_Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaProvaRegina
{
    public class Location : FastPriorityQueueNode
    {
        public int X;
        public int Y;
        public double F;
        public double G;
        public double H;
        public Location Parent;
        public Location()
        {
            
        }
        public override bool Equals(object other)
        {
            var otherNode = other as Location;
            if (otherNode.X == this.X && otherNode.Y == this.Y)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 597 + X.GetHashCode();
                hash = hash * 1173 + Y.GetHashCode();

                return hash;
            }
        }
        public override string ToString()
        {
            return string.Format("[{0};{1}]", this.X, this.Y);
        }
    }
}
