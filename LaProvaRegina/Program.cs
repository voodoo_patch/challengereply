﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Scanning;
using TriangulatedPolygonAStar.BasicGeometry;
using Priority_Queue;
using TriangulatedPolygonAStar;
using System.Diagnostics;

namespace LaProvaRegina
{

    public static class Program
    {
        static int N = 1000000;
        static int D = 1;
        static int D2 = 1;
        static int P1 = 1;
        static int P2 = 2;


        static void Main(string[] args)
        {
            CustomStopwatch sw = new CustomStopwatch();
            sw.Start();
            Random r = new Random();
            HashSet<Triangle> lTriangles = new HashSet<Triangle>();
            List<Location> map = new List<Location>();
            

            #region Import da file
            object[] targets = new object[6];
            targets[0] = new Int32();
            targets[1] = new Int32();
            targets[2] = new Int32();
            targets[3] = new Int32();
            targets[4] = new Int32();
            targets[5] = new Int32();

            Scanner scanner = new Scanner();
            Location current = null;


            var lines = File.ReadLines(@"input.txt").ToList();
            scanner.Scan(lines[0],
             "{0} {1} {2} {3}", targets);

            int StartX = Convert.ToInt32(targets[0]);
            int StartY = Convert.ToInt32(targets[1]);
            Location start = new Location { X = StartX, Y = StartY };
            map.Add(start);
            int GoalX = Convert.ToInt32(targets[2]);
            int GoalY = Convert.ToInt32(targets[3]);
            Location target = new Location { X = GoalX, Y = GoalY };
            map.Add(target);
            scanner.Scan(lines[1],
             "{0}", targets);
            var nTriangles = Convert.ToDouble(targets[0]);
            for (int i = 2; i < nTriangles + 2; i++)
            {
                scanner.Scan(lines[i],
             "{0} {1} {2} {3} {4} {5}", targets);
                var a = new Vector(Convert.ToDouble(targets[0]), Convert.ToDouble(targets[1]));
                var b = new Vector(Convert.ToDouble(targets[2]), Convert.ToDouble(targets[3]));
                var c = new Vector(Convert.ToDouble(targets[4]), Convert.ToDouble(targets[5]));
                var t = new Triangle(a, b, c, i - 2);
                lTriangles.Add(new Triangle(a, b, c, i - 2));
            }
            #endregion
            sw.Stop();
            Console.WriteLine("Fine generaz mappa elapsed: {0}, StartAt: {1}, EndAt: {2}", sw.ElapsedMilliseconds, sw.StartAt.Value, sw.EndAt.Value);
            Console.WriteLine("\n");
            double SQRT2 = Math.Sqrt(2);

            //target.X = 1000;
            //target.Y = 1000;
            var step = 0;
            Dictionary<string, Location> closedSet = new Dictionary<string, Location>();
            Dictionary<string, Location> openSet = new Dictionary<string, Location>();
            Dictionary<string, Location> cameFrom = new Dictionary<string, Location>();
            Dictionary<string, double> gScore = new Dictionary<string, double>();
            Dictionary<string, double> fScore = new Dictionary<string, double>();
            if (true)
            {
                
                AssaignScore(start, 0.0, ref gScore);
                AssaignScore(start, ComputeHScore(start,target,start), ref fScore);                
                openSet.Add(start.ToString(), start);
                var frontier = new FastPriorityQueue<Location>(1000000);
                //var frontier = new PriorityQueue<Location>();
                frontier.Enqueue(start, 0);
                Dictionary<string, Location> visitati = new Dictionary<string, Location>();
                visitati.Add(start.ToString(), start);
                //cameFrom[start.ToString()] = start;
                

                while (openSet.Count > 0)
                {
                    current = frontier.Dequeue();
                    //double lowest = openSet.Min(l => fScore[l.ToString()]);
                    //var min = double.MaxValue;
                    //foreach (var item in openSet)
                    //{
                    //    if (fScore[item.Value.ToString()] < min)
                    //    {
                    //        min = fScore[item.Value.ToString()];
                    //        current = item.Value;
                    //    }
                    //}
                    if (current.Equals(target))
                    {
                        break;
                    }
                    Console.WriteLine((step++)+":" + current + ", G:" + gScore[current.ToString()] + ", F:" + fScore[current.ToString()]);
                    //current = openSet.OrderBy(l => fScore[l.Value.ToString()],).First();
                    openSet.Remove(current.ToString());
                    closedSet.Add(current.ToString(), current);
                    
                    


                    foreach (var next in GetWalkableAdjacentSquares(current.X, current.Y, start, target, ref lTriangles, ref visitati, gScore[current.ToString()]))
                    {                    
                        if (closedSet.ContainsKey(next.ToString()))
                            continue;

                        if (IsPointDentroTriangoliv3(next.X, next.Y, ref lTriangles))
                        {
                            closedSet.Add(next.ToString(),next);
                            continue;
                        }
                        

                        if(!gScore.ContainsKey(next.ToString()))
                            AssaignScore(next, double.MaxValue, ref gScore);
                        double tentative_gScore = gScore[current.ToString()] + ((next.X - current.X == 0 || next.Y - current.Y == 0) ? 1 : SQRT2);

                        if (tentative_gScore >= gScore[next.ToString()])
                            continue;

                        if (!openSet.ContainsKey(next.ToString()))
                            openSet.Add(next.ToString(), next);

                        var nodeInOpen = openSet[next.ToString()];
                        cameFrom[nodeInOpen.ToString()] = current;
                        AssaignScore(nodeInOpen, tentative_gScore, ref gScore);
                        AssaignScore(nodeInOpen, tentative_gScore + ComputeHScore(nodeInOpen, target, start), ref fScore);
                        var newPriority = Convert.ToSingle(fScore[nodeInOpen.ToString()]);
                        if (!frontier.Contains(nodeInOpen))
                            frontier.Enqueue(nodeInOpen, newPriority);
                        else
                            frontier.UpdatePriority(nodeInOpen, newPriority);


                        //costSoFar[next.ToString()] = newCost;
                        //double priority = newCost>500? newCost+ ComputeHScore(next.X, next.Y, target.X, target.Y, start.X, start.Y) 
                        //    : ComputeHScore(next.X, next.Y, target.X, target.Y, start.X, start.Y);                            
                        
                        
                    }
                }
            }
            else
            {
                #region oldversion
                var openList = new HashSet<Location>();
                var closedList = new HashSet<Location>();
                var WallList = new HashSet<Location>();
                int g = 0;
                sw = new CustomStopwatch();
                sw.Start();
                // start by adding the original position to the open list
                start.G = 0;
                start.H = 0;
                start.F = 0;
                openList.Add(start);
                while (openList.Count > 0)
                {
                    // algorithm's logic goes here
                    // get the square with the lowest F score
                    var lowest = openList.AsParallel().Min(l => l.F);
                    current = openList.AsParallel().First(l => l.F == lowest);
                    
                    // add the current square to the closed list
                    closedList.Add(current);

                    // remove it from the open list
                    openList.Remove(current);


                    if (current.X == target.X && current.Y == target.Y)
                    {
                        Console.WriteLine("Trovato");
                        break;
                    }

                    //var adjacentSquares = GetWalkableAdjacentSquares(current.X, current.Y, start, target, ref lTriangles);
                    var adjacentSquares = new List<Location>();
                    g++;


                    if (g % 1 == 0)
                    {
                        //Console.SetCursorPosition(0, 0);
                        //Console.Write(g);
                        //Console.SetCursorPosition(10, 10);
                        Console.WriteLine(current.X + " ; " + current.Y + "    F:" + current.F + ", G:" + current.G + ", H:" + current.H + "       g=" + g);
                    }
                    foreach (var adjacentSquare in adjacentSquares)
                    {
                        if (IsPointDentroTriangoliv2(adjacentSquare.X, adjacentSquare.Y, ref lTriangles))
                        {
                            //closedList.Add(new Location() { X = adjacentSquare.X, Y = adjacentSquare.Y, F = double.MaxValue });
                            WallList.Add(new Location() { X = adjacentSquare.X, Y = adjacentSquare.Y, F = double.MaxValue });

                        }
                        else
                        {
                            double newg = current.G + ((adjacentSquare.X - current.X == 0 || adjacentSquare.Y - current.Y == 0) ? 1 : SQRT2);
                            // if this adjacent square is already in the closed list, ignore it
                            if (closedList.AsParallel().WithDegreeOfParallelism(8).FirstOrDefault(l => l.X == adjacentSquare.X
                                    && l.Y == adjacentSquare.Y) != null)
                                continue;
                            var next = openList.AsParallel().WithDegreeOfParallelism(8).FirstOrDefault(l => l.X == adjacentSquare.X
                                      && l.Y == adjacentSquare.Y);

                            // if it's not in the open list...
                            if (next == null || newg < next.G)
                            {
                                // compute its score, set the parent
                                adjacentSquare.G = newg;
                                adjacentSquare.H = ComputeHScore(adjacentSquare.X,
                                    adjacentSquare.Y, target.X, target.Y, start.X, start.Y);
                                adjacentSquare.F = adjacentSquare.G + adjacentSquare.H;
                                adjacentSquare.Parent = current;

                                // and add it to the open list
                                openList.Add(adjacentSquare);
                                //openlistPQ.Enqueue(adjacentSquare, adjacentSquare.F);
                            }

                        }
                    }
                }
                #endregion
            }
            Console.WriteLine("\n");
            Console.BackgroundColor = ConsoleColor.Red;
            #region Stampa su file la soluzione
            int zompi = 0;

            Console.WriteLine("costo: " + gScore[current.ToString()]);
            FileStream ostrm;
            StreamWriter writer;
            TextWriter oldOut = Console.Out;
            try
            {
                ostrm = new FileStream("./output.txt", FileMode.Create, FileAccess.Write);                
                writer = new StreamWriter(ostrm);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open Redirect.txt for writing");
                Console.WriteLine(e.Message);
                return;
            }
            Console.SetOut(writer);


            // assume path was found; let's show it
            while (cameFrom[current.ToString()] != start)
            {
                zompi++;
                //Console.SetCursorPosition(current.X, current.Y);
                //Console.Write('_');
                //Console.SetCursorPosition(current.X, current.Y);
                Console.WriteLine(current.X+","+ current.Y);
                //map[current.X,current.Y]= Shortest;
                current = cameFrom[current.ToString()];
                //System.Threading.Thread.Sleep(50);
                //Console.ReadLine();
            }
            Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();
            #endregion
            Console.WriteLine("Numero zompi: " + zompi);
            sw.Stop();
            Console.WriteLine("Stopwatch elapsed: {0}, StartAt: {1}, EndAt: {2}", sw.ElapsedMilliseconds, sw.StartAt.Value, sw.EndAt.Value);



            Console.WriteLine("Done");
            string curDir = Directory.GetCurrentDirectory();
            
            Process.Start(String.Format("file:///{0}/TRY_0.html", curDir));
            System.Threading.Thread.Sleep(30000000);
        }

        private static double ComputeHScore(Location node, Location target, Location start)
        {
            return ComputeHScore(node.X, node.Y, target.X, target.Y, start.X, start.Y);
        }

        private static void AssaignScore(Location node, double v, ref Dictionary<string, double> gScore)
        {
            if (!gScore.ContainsKey(node.ToString()))
            {               
                gScore.Add(node.ToString(), v);
            }
            else
            {
                gScore[node.ToString()] = v;
            }
        }

        



        /// <summary>
        /// stampa della soluzione partenda dal target risale i figli
        /// </summary>
        /// <param name="current">nodo target</param>
        private static void Stampamappav3(Location current)
        {
            FileStream ostrm;
            StreamWriter writer;
            TextWriter oldOut = Console.Out;
            try
            {
                ostrm = new FileStream("./output.txt", FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open Redirect.txt for writing");
                Console.WriteLine(e.Message);
                return;
            }
            Console.SetOut(writer);


            // assume path was found; let's show it
            while (current != null)
            {
                //Console.SetCursorPosition(current.X, current.Y);
                //Console.Write('_');
                //Console.SetCursorPosition(current.X, current.Y);
                Console.WriteLine(current.X + ", " + current.Y);
                //map[current.X,current.Y]= Shortest;
                current = current.Parent;
                //System.Threading.Thread.Sleep(50);
                //Console.ReadLine();
            }

            Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();
        }

        /// <summary>
        /// stampa gli ostacoli
        /// </summary>
        /// <param name="t">lista triangoli</param>
        private static void Stampamappav2(ref List<Triangle> t)
        {
            FileStream ostrm;
            StreamWriter writer;
            TextWriter oldOut = Console.Out;
            try
            {
                ostrm = new FileStream("./provatissima.txt", FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open Redirect.txt for writing");
                Console.WriteLine(e.Message);
                return;
            }
            Console.SetOut(writer);
            for (int i = 0; i < 300; i++)
            {
                for (int j = 0; j < 300; j++)
                {
                    if (IsPointDentroTriangoli(j, i, ref t))
                        Console.Write("X");
                    else
                        Console.Write(" ");

                }
                Console.Write('\n');
            }
            Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();
        }


        /// <summary>
        /// Stampa una lista di location ad esempio closed list o openlist 
        /// </summary>
        /// <param name="map"></param>
        private static void Stampamappa(List<Location> map)
        {
            FileStream ostrm;
            StreamWriter writer;
            TextWriter oldOut = Console.Out;
            try
            {
                ostrm = new FileStream("./provatissima.txt", FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open Redirect.txt for writing");
                Console.WriteLine(e.Message);
                return;
            }
            Console.SetOut(writer);
            for (int i = 0; i < 1200; i++)
            {
                for (int j = 0; j < 1200; j++)
                {
                    if (map.Any(x => x.X == j && x.Y == i))
                        Console.Write("X");
                    else
                        Console.Write(" ");

                }
                Console.Write('\n');
            }
            Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();
        }
        /// <summary>
        /// Stampa una lista di location ad esempio closed list o openlist 
        /// </summary>
        /// <param name="map"></param>
        private static void Stampamappav4(HashSet<Location> map)
        {
            FileStream ostrm;
            StreamWriter writer;
            TextWriter oldOut = Console.Out;
            try
            {
                ostrm = new FileStream("./provatissima.txt", FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open Redirect.txt for writing");
                Console.WriteLine(e.Message);
                return;
            }
            Console.SetOut(writer);
            for (int i = 0; i < 1000; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    if (map.Where(x => x.X == j && x.Y == i).Any())
                        Console.Write("X");
                    else
                        Console.Write(" ");

                }
                Console.Write('\n');
            }
            Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();
        }

        /// <summary>
        /// Metodo lentooo per trovare se point in triangles
        /// </summary>
        /// <param name="j"></param>
        /// <param name="k"></param>
        /// <param name="lTriangles"></param>
        /// <returns></returns>
        public static bool IsPointDentroTriangoli(int j, int k, ref List<Triangle> lTriangles)
        {
            foreach (Triangle t in lTriangles)
            {
                Vector v = new Vector(j, k);
                if (t.ContainsPoint(v))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// PIT buonoooo
        /// </summary>
        /// <param name="j"></param>
        /// <param name="k"></param>
        /// <param name="lTriangles"></param>
        /// <returns></returns>
        public static bool IsPointDentroTriangoliv2(int j, int k, ref HashSet<Triangle> lTriangles)
        {
            var triangoliBuoni = lTriangles.AsParallel().WithDegreeOfParallelism(8)
                .Where(t =>
                    !((j > t.A.X && j > t.B.X && j > t.C.X) ||
                    (j < t.A.X && j < t.B.X && j < t.C.X) ||
                    (k > t.A.Y && k > t.B.Y && k > t.C.Y) ||
                    (k < t.A.Y && k < t.B.Y && k < t.C.Y))
                    );
            foreach (Triangle t in triangoliBuoni)
            {
                //Vector v = new Vector(j, k);
                //if (t.ContainsPoint(v))
                //    return true;
                //Vector v = new Vector(j, k);                
                if (ptInTriangle(j, k, t.A.X, t.A.Y, t.B.X, t.B.Y, t.C.X, t.C.Y))
                    return true;
            }
            return false;
        }
        public static bool ptInTriangle(double x, double y, double x0, double y0, double x1, double y1, double x2, double y2)
        {
            var A = 0.5 * (-y1 * x2 + (y0 * (-x1 + x2)) + (x0 * (y1 - y2)) + x1 * y2);
            var sign = A < 0 ? -1 : 1;
            var s = (y0 * x2 - x0 * y2 + (y2 - y0) * x + (x0 - x2) * y) * sign;
            var t = (x0 * y1 - y0 * x1 + (y0 - y1) * x + (x1 - x0) * y) * sign;

            return s > 0 && t > 0 && (s + t) < 2 * A * sign;
        }

        public static bool ptInTrianglev2(double x, double y, double x1, double y1, double x2, double y2, double x3, double y3)
        {
            double a = ((y2 - y3) * (x - x3) + (x3 - x2) * (y - y3)) / ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3));
            double b = ((y3 - y1) * (x - x3) + (x1 - x3) * (y - y3)) / ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3));
            double c = 1 - a - b;

            if (a == 0 || b == 0 || c == 0)
                return true;
            else
                return false;
        }

        private static double ComputeHScoreSimple(int x, int y, int targetX, int targetY)
        {
            return Math.Abs(targetX - x) + Math.Abs(targetY - y);
        }
        public static double ComputeHScore(int x, int y, int targetX, int targetY, int startX, int startY)
        {
            int dx = Math.Abs(targetX - x);
            int dy = Math.Abs(targetY - y);
            
            int Orthogonal = Math.Abs(dx - dy);
            int Diagonal = Math.Abs(((dx + dy) - Orthogonal) / 2);
            return 2 * (Diagonal + Orthogonal + dx + dy);
            //return 1.5*Math.Sqrt(dx * dx + dy * dy);
            //return D * (dx + dy) + (D2 - 2 * D) * Math.Min(dx, dy);
            //con ostacoli
            double heuristic = D * (dx + dy) + (D2 - 2 * D) * Math.Min(dx, dy);
            int dx1 = Math.Abs(x - targetX);
            int dy1 = y - targetY;
            int dx2 = startX - targetX;
            int dy2 = startY - targetY;
            double cross = Math.Abs(dx1 * dy2 - dx2 * dy1);
            heuristic += cross * 0.001;

            return heuristic;
            //manhatta
            //return Math.Abs(targetX - x) + Math.Abs(targetY - y);
        }
        public static List<Location> GetWalkableAdjacentSquares(int x, int y, Location start, Location target, ref HashSet<Triangle> triangles, ref Dictionary<string, Location> visitati, double costoCurrent=0)
        {
            //var proposedLocations = new List<Location>();

            var proposedLocations = new List<Location>();
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (i == j && i == 0)
                    {
                        continue;
                    }
                    var node = new Location() { X = x + i, Y = y + j };
                    //NON FUNZIONA BENE mi ritrovo sempre gli stessi current dopo un po'
                    if (!visitati.TryGetValue(node.ToString(), out node))
                    {
                        node = new Location() { X = x + i, Y = y + j };
                        visitati.Add(node.ToString(), node);
                    }
                    //if (node.G == double.MaxValue)
                    //{
                    //    //questo è un muro non lo aggiungo ai walkable
                    //    continue;
                    //}
                    //if (IsPointDentroTriangoliv2(node.X, node.Y, ref triangles))
                    //{
                    //    node.G = double.MaxValue;
                    //    continue;
                    //}
                    //node.H = ComputeHScore(node.X, node.Y, target.X, target.Y, start.X, start.Y);
                    proposedLocations.Add(node);


                }
            }

            //if (!IsPointDentroTriangoliv2(x, y - 1, ref triangles))
            //    proposedLocations.Add(new Location { X = x, Y = y - 1,});
            //if (!IsPointDentroTriangoliv2(x, y + 1, ref triangles))
            //    proposedLocations.Add(new Location { X = x, Y = y + 1 });
            //if (!IsPointDentroTriangoliv2(x-1, y, ref triangles))
            //    proposedLocations.Add(new Location { X = x - 1, Y = y });
            //if (!IsPointDentroTriangoliv2(x+1, y, ref triangles))
            //    proposedLocations.Add(new Location { X = x + 1, Y = y });

            //if (!IsPointDentroTriangoliv2(x+1, y +1, ref triangles))
            //    proposedLocations.Add(new Location { X = x + 1, Y = y + 1 });
            //if (!IsPointDentroTriangoliv2(x+1, y - 1, ref triangles))
            //    proposedLocations.Add(new Location { X = x + 1, Y = y - 1 });
            //if (!IsPointDentroTriangoliv2(x-1, y + 1, ref triangles))
            //    proposedLocations.Add(new Location { X = x - 1, Y = y + 1 });
            //if (!IsPointDentroTriangoliv2(x-1, y - 1, ref triangles))
            //    proposedLocations.Add(new Location { X = x - 1, Y = y - 1 });

            //proposedLocations.ForEach(z => z.H = ComputeHScore(z.X, z.Y, target.X, target.Y,start.X,start.Y));
            //var prop= proposedLocations.OrderBy(z => z.H).Take(1).ToList();
            return proposedLocations.OrderBy(z => z.H).ToList();
        }
        public static bool IsPointDentroTriangoliv3(int j, int k, ref HashSet<Triangle> lTriangles)
        {
            return lTriangles.AsParallel()
                .WithDegreeOfParallelism(8)
                .Any(t => PointInTriangle(t.A, t.B, t.C, new Vector(j, k)));
        }

        public static bool PointInTriangle(IVector A, IVector B,  IVector C,  IVector P)
        {
            var _a = new System.Numerics.Vector3((float)A.X,(float)A.Y,0);
            var _b = new System.Numerics.Vector3((float)B.X,(float)B.Y,0);
            var _c = new System.Numerics.Vector3((float)C.X, (float)C.Y, 0);
            var _p = new System.Numerics.Vector3((float)P.X, (float)P.Y, 0);
            return PointInTriangle3(ref _a,ref _b,ref _c,ref _p);
        }
        ///<summary>
        /// Determine whether a point P is inside the triangle ABC. Note, this function
        /// assumes that P is coplanar with the triangle.
        ///</summary>
        ///<returns>True if the point is inside, false if it is not.</returns>
        public static bool PointInTriangle3(ref System.Numerics.Vector3 A, ref System.Numerics.Vector3 B, ref System.Numerics.Vector3 C, ref System.Numerics.Vector3 P)
        {
            // Prepare our barycentric variables
            System.Numerics.Vector3 u = B - A;
            System.Numerics.Vector3 v = C - A;
            System.Numerics.Vector3 w = P - A;

            System.Numerics.Vector3 vCrossW = System.Numerics.Vector3.Cross(v, w);
            System.Numerics.Vector3 vCrossU = System.Numerics.Vector3.Cross(v, u);

            // Test sign of r
            if (System.Numerics.Vector3.Dot(vCrossW, vCrossU) < 0)
                return false;

            System.Numerics.Vector3 uCrossW = System.Numerics.Vector3.Cross(u, w);
            System.Numerics.Vector3 uCrossV = System.Numerics.Vector3.Cross(u, v);

            // Test sign of t
            if (System.Numerics.Vector3.Dot(uCrossW, uCrossV) < 0)
                return false;

            // At this point, we know that r and t and both > 0.
            // Therefore, as long as their sum is <= 1, each must be less <= 1
            float denom = uCrossV.Length();
            float r = vCrossW.Length() / denom;
            float t = uCrossW.Length() / denom;

            return (r + t <= 1);
        }

    }

    



}

namespace System
{
    public static class Extensions
    {
        public static string ReplaceAt(this string input, int index, char newChar)
        {
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }
            StringBuilder builder = new StringBuilder(input);
            builder[index] = newChar;
            return builder.ToString();
        }
    }

}